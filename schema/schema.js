const Joi = require('joi');

const productID = Joi.object({

    'id' : Joi.number().min(1).max(500).required()
});
const userParams = Joi.object({

    'id' : Joi.string().required(),
    'password': Joi.string().required(),
    'email':Joi.string().email().required(),
    'phone':Joi.number(),
    'location':Joi.string(),
    'name':Joi.string()
});
const Message = Joi.object({

    'message' : Joi.string().required(),
    'senderID': Joi.string(),
    'receiverID': Joi.string(),
    'pid': Joi.string(),
    'type':Joi.string(),
    'points':Joi.number(),
    'product':Joi.string()
});

module.exports = {
    productID,
    userParams,
    Message

}