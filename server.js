'use strict';
const Hapi = require('hapi');
const r = require('rethinkdb');
const Joi = require('joi');
const fs = require('fs');
const Inert = require('inert');
const Vision = require('vision');
const HapiSwagger = require('hapi-swagger');
const Pack = require('./package');
const Routes = require('./routes/all.js');


const server = new Hapi.Server();

server.connection({ 
    host: 'localhost', 
    port: 3000,
    routes: { cors: true } 
});

const options = {
    info: {
        'title': 'Test API Documentation',
        'version': Pack.version
    }
};
server.register([
    Inert,
    Vision,
    {
        'register': HapiSwagger,
        'options': options
    }
], (err) => {
        server.start((err) => {
            if (err) {
                console.log(err);
            } else {
                console.log('Server running at:', server.info.uri);
                
            }
        });

        server.route(Routes);
});


