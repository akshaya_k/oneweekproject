
"use strict";
const db = require("./db-connection.js");
const co = require("co");
var thinky = require('thinky')({
    db: 'eCommerce3'
});
var r = thinky.r;
var _ = require('lodash');
var type = thinky.type;
var Errors = thinky.Errors;

class userModel {

    constructor() {

        console.log("User Constructor Called");


    }

    checkForUser(id) {

        if(!id || typeof(id)!="string")
            return false;

        return co(function*() {
            try {
                var user = yield db.User.get(id);
                return true;
            } catch (err) {

                return false;
            }

        });
    }

    signUp(body) {

        let me = this;
        if(!body || typeof(body)!="object")
            return false;
       
        return co(function*() {
            try {

                var ifIdExists = yield me.checkForUser(body.id);

                if (ifIdExists)
                    return false;

                else {

                    if (body.id == undefined || body.email == undefined || body.password == undefined || typeof(body.phone)=="string" )
                        return false;
                    

                    var u = {
                        "id": body.id,
                        "name": body.name,
                        "password": body.password,
                        "email": body.email,
                        "phone": body.phone,
                        "location": body.location,
                        "badge": "Novice",
                        "points": 0
                    };

                    var isSaved = yield me.populateUsers(u);
                    if (isSaved)
                        return true;
                    else
                        return false;
                }
            } catch (err) {

                return false;
            }

        });
    }

    populateUsers(u) {

        if(!u)
            return false;
        
        if(typeof(u)!="object")
            return false;
        

        return co(function*() {

            try {
                var isSaved = yield db.User.save(u);
                return true;
            } catch (err) {
                return false;
            }

        });
    }


    validUser(username, password) {

        let me = this;
        if (username == undefined || password == undefined)
            return false;

        return co(function*() {

            try {

                var ur = yield db.User.get(username).run();
                if (ur.password == password)
                    return true;
                else
                    return false;

            } catch (err) {
                return false;
            }
        });

    }

    sendMessage(data) {

        if (data == undefined)
            return false;

         if (typeof(data) != "object")
            return false;

        if (!data.receiverID || !data.senderID || !data.type || !data.message)
            return false;

        if (data.receiverID == data.senderID)
            return false;

        let me = this;
        

        return co(function*() {

            try {

                var senderID = yield me.checkForUser(data.senderID);
                
                var receiverID = yield me.checkForUser(data.receiverID);
                

                if (!senderID || !receiverID)
                     return false;

                var id = yield r.uuid().run();

                if (!id)
                    return false;

                data.id = id;
                data.pid = id;
                data.date = new Date();
                data.status = "Unresolved";

                var result = yield db.Message.save(data);
                if (!result)
                    return false;

                return result.message;

            } catch (err) {
                return false;
            }


        });


    }

    shareMessage(pid, data) {

        if (!data || !pid)
            return false;

         if (typeof(data)!="object" || typeof(pid)!="string")
            return false;

        if (!data.receiverID || !data.senderID || !data.type)
            return false;

        if (data.receiverID == data.senderID)
            return false;

        let me=this;

        return co(function*() {

            try {

                var senderID = yield me.checkForUser(data.senderID);
                var receiverID = yield me.checkForUser(data.receiverID);

                if (!senderID || !receiverID)
                    return false;

                var id = yield r.uuid().run();
                if (!id)
                    return false;

                data.id = id;
                data.pid = id;
                data.date = new Date();
                data.status = "Unresolved";
                data.message = "http://localhost:3000/#/product/" + pid;

                var result = yield db.Message.save(data);
                if (!result)
                    return false;

                return result.message;

            } catch (err) {
                return false;
            }

        });


    }

    replyMessage(data) {

        if (!data)
            return false;

        if(typeof(data)!="object")
            return false;

        if (!data.receiverID || !data.senderID || !data.type)
            return false;

        if (data.receiverID == data.senderID)
            return false;
        
        let me = this;

        return co(function*() {

            try {

                var senderID = yield me.checkForUser(data.senderID);
                var receiverID = yield me.checkForUser(data.receiverID);

                if (!senderID || !receiverID)
                    return false;

                var id = yield r.uuid().run();

                if (!id)
                    return false;

                data.id = id;
                data.date = new Date();
                data.status = "Unresolved";

                var isSaved = yield db.Message.save(data);

                if (!isSaved)
                    return false;

                var points = yield db.User.get(data.senderID).pluck('points', 'badge').run();

                if (!points)
                    return false;

                var pp = points.points + data.points;
                delete data.points;

                var getUser;
                
                if (pp > 500 && points.badge != "Expert")
                        getUser = yield db.User.get(data.senderID).update({
                                points: pp,
                                badge: 'Expert'
                            });
                   

                else
                    getUser = yield db.User.get(data.senderID).update({
                            points: pp
                        });
                   
                

                if (!getUser)
                    return false;

                return data.message;

            } catch (err) {
                return false;
            }

        });


    }

    getPoints(id) {

        if (id == undefined)
            return {
                badge: 'Unavailable',
                points: -1
            };

        return co(function*() {

            try {
                var points = yield db.User.get(id).pluck('points', 'badge').run()
                return points;

            } catch (err) {
                return {
                    badge: 'Unavailable',
                    points: -1
                };
            }
        });

    }

    getAllMessage(senderID) {

        if(!senderID || typeof(senderID)!="string")
            return false;

        let me= this;

        return co(function*() {

            try {
                var senderid = yield me.checkForUser(senderID);
                if(!senderid)
                    return false;

                var result = yield db.Message.filter(r.row('senderID').eq(senderID).or(r.row('receiverID').eq(senderID))).group('pid').orderBy('type', 'date').run();
                return result;

            } catch (err) {
                return false;
            }


        });


    }

    resolveMessage(pid) {

        if(!pid || typeof(pid)!="string")
            return false;

        return co(function*() {

            try {
                var filterPID = yield db.Message.filter({
                    pid: pid
                }).update({
                    status: "Resolved"
                });

                if(!filterPID || filterPID.length <= 0)
                    return false;

                else
                    return true;

            } catch (err) {

                return false;
            }

        });


    }

    unresolveMessage(pid) {

        if(!pid || typeof(pid)!="string")
            return false;
        
        return co(function*() {

            try {

                var filterPID = yield db.Message.filter({
                    pid: pid
                }).update({
                    status: "Unresolved"
                });

                 if(!filterPID || filterPID.length <= 0)
                    return false;

                else
                    return true;
            
            } catch (err) {
                return false;
            }
        });


    }


    ifBought(userID, productID) {

        if(!userID || !productID || typeof(userID)!="string" || typeof(productID)!="string")
            return false;

        return co(function*() {
            try {
                var points = yield db.Preferences.filter({
                    uid: userID,
                    type: 'buy',
                    pid: Number(productID)
                }).run()
                return points;
            } catch (err) {
                return false;
            }
        });

    }

    getExperts(pid, uid) {

        if(!pid || !uid || typeof(pid)!="string" || typeof(uid)!="string")
            return false;


        return co(function*() {

            try {
                var users = [];
                var usersBought = yield db.Preferences.filter({
                    pid: Number(pid),
                    type: 'buy'
                }).filter(r.row("uid").ne(uid)).pluck('uid').coerceTo('array').run();

                if (!usersBought)
                    return false;

                for (var i = 0; i < usersBought.length; i++)
                    users.push({
                        id: usersBought[i].uid
                    });


                var usersExpert = yield db.User.filter({
                    badge: 'Expert'
                }).filter(r.row("id").ne(uid)).pluck('id').coerceTo('array').run();

                if (!usersExpert)
                    return false;

                for (var i = 0; i < usersExpert.length; i++)
                    users.push({
                        id: usersExpert[i].id
                    });


                return users;

            } catch (err) {
                return false;
            }
        });

    }

    getUsers(excludeID) {

         if(!excludeID || typeof(excludeID)!="string")
            return false;

        return co(function*() {
            try {
                var users = yield db.User.pluck('id').filter(r.row("id").ne(excludeID)).orderBy('id').coerceTo('array').run()
                return users;
            } catch (err) {
                return false;
            }
        });

    }

    getOneQuestion(uid, type) {

        //type means Weekly / Daily

        if(!uid || !type || typeof(uid)!="string" || typeof(type) !="string" || (type!="Weekly" && type!="Daily"))
            return false;
            
        return co(function*() {

            try {

                var list = [];
                var list1 = [];
                var list2 = [];
                var list3 = [];

                var Bought = yield db.Preferences.filter({
                    type: 'buy',
                    uid: uid
                }).coerceTo('array').run();

                if (Bought)
                    for (var i = 0; i < Bought.length; i++) {
                        list2.push(Bought[i].pid);
                    }

                var Answers = yield db.Answer.filter(function(doc) {
                    return r.expr(list2).contains(doc("pid"))
                }).filter({
                    uid: uid,
                    type: type
                }).pluck('qid');

                if (Answers)
                    for (var i = 0; i < Answers.length; i++) {
                        list1.push(Answers[i].qid);
                    }

                var shortlistedQ = yield db.Question.filter(function(doc) {
                    return r.expr(list2).contains(doc("pid"))
                }).filter({
                    type: type
                }).run()

                if (shortlistedQ)
                    for (var i = 0; i < shortlistedQ.length; i++) {
                        list3.push(shortlistedQ[i].id);
                    }

                if (list3.length == 0 && list1.length == 0)
                    return "No Products Bought yet!";


                var finalList = _.difference(list3, list1);

                if (finalList.length == 0)
                    return "All Questions Answered";

                var oneQuestion = yield db.Question.get(finalList[0]).run();
                var oneQuestionProductId = yield db.Product.get(oneQuestion.pid).run();

                oneQuestion.name = oneQuestionProductId.model + " " + oneQuestionProductId.category;
                return oneQuestion;
            } catch (err) {
                return false;
            }

        });

    }


    answerOneQuestion(body) {

        if(!body || typeof(body)!="object")
                    return false;

        let me = this;

        if (!body.uid || !body.message || !body.qid)
            return false;
        

        return co(function*() {

            try {

                

                var answererID = yield me.checkForUser(body.uid);

                if (!answererID)
                    return false;

                var ans = {};
                ans.id = yield r.uuid().run();
                ans.qid = body.qid;
                ans.uid = body.uid;
                ans.answer = body.message;
                
                var res = yield db.Question.get(Number(body.qid)).run();
                
                ans.type = res.type;
                ans.points = res.points;
                ans.pid = res.pid;
                ans.date = new Date();

                var points = yield db.User.get(body.uid).pluck('points').run();
                
                var pp = points.points + res.points;
                
                if (pp > 500)
                
                    var result2 = yield db.User.get(body.uid).update({
                        points: pp,
                        badge: 'Expert'
                    });
                
                else
                    var result2 = yield db.User.get(body.uid).update({
                        points: pp
                    });
                
                var x = yield db.Answer.save(ans);
                
                if (x)
                    return true;
                
                else
                    return "Unable to Process Answer.";
            
            } 
            catch (err) {
                    return "Unable to Process Answer.";
            }

        });
    }


    getQuestion(id) {

        return co(function*() {
            var question = yield db.Question.get(id).pluck('question', 'points').run()
            return question;
        });

    }

    getProduct(id) {

        return co(function*() {
            var product = yield db.Product.get(id).pluck('model', 'category').run()
            return product;
        });

    }

    getAllAnswers(uid) {

        let me = this;
        if(!uid || typeof(uid)!="string")
            return false;

        return co(function*() {

            try{
                var lists = [];
                var answers = yield db.Answer.filter({
                    uid: uid
                }).orderBy('pid').coerceTo('array').run()
                for (var i = 0; i < answers.length; i++) {
                    var qname = yield me.getQuestion(answers[i].qid);
                    var pname = yield me.getProduct(answers[i].pid);
                    lists.push({
                        pp: qname.points,
                        q: qname.question,
                        a: answers[i].answer,
                        p: pname.model,
                        c: pname.category
                    });
                }
                return lists;
            }
            catch(err){
                return false;
            }
        });

    }

}

let U = new userModel();
module.exports = U;