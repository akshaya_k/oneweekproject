
"use strict";
const co = require("co");
var thinky = require('thinky')({
    db: 'eCommerce3'
});
var r = thinky.r;
var type = thinky.type;
var Errors = thinky.Errors;

class DB {

    constructor() {

        console.log("DB Constructor Called");
        this.User = thinky.createModel("User", {
            id: type.string(),
            name: type.string(),
            password: type.string(),
            email: type.string(),
            phone: type.number(),
            location: type.string(),
            badge: type.string(),
            points: type.number()
        });

        this.Product = thinky.createModel("Product", {
            id: type.number(),
            model: type.string(),
            series: type.string(),
            category: type.string(),
            size: type.string(),
            color: type.string(),
            price: type.number(),
            image: type.string(),
            sold: type.number(),
        });
        this.Preferences = thinky.createModel("Preferences", {
            id: type.string(),
            uid: type.string(),
            pid: type.number(),
            type: type.string(),
            date: type.date()
        });
        this.Message = thinky.createModel("Message", {
            id: type.string(),
            senderID: type.string(),
            receiverID: type.string(),
            status: type.string(), //-( resolved/unresolved)
            type: type.string(), //- answer/ask/share/order
            message: type.string(),
            pid: type.string(), //parent thread
            date: type.date()
        });
        this.Question = thinky.createModel("Question", {
            id: type.number(),
            pid: type.number(),
            question: type.string(),
            type: type.string(),
            points: type.number()
        });
        this.Answer = thinky.createModel("Answer", {
            id: type.string(),
            qid: type.number(),
            pid: type.number(),
            answer: type.string(),
            uid: type.string(),
            type: type.string(),
            points: type.number(),
            date: type.date()
        });
        
        this.User.hasMany(this.Preferences, "prefer", "id", "uid");
        this.Preferences.belongsTo(this.User, "user", "uid", "id");
        this.Preferences.belongsTo(this.Product, "product", "pid", "id");
        this.Question.belongsTo(this.Product, "product", "pid", "id");

    }



}

let dB = new DB();
module.exports = dB;