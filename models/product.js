
"use strict";
const db = require("./db-connection.js");
const co = require("co");
var thinky = require('thinky')({
    db: 'eCommerce3'
});
var r = thinky.r;
var type = thinky.type;
var Errors = thinky.Errors;

class productModel {

    constructor() {

        console.log("Product Constructor Called");

    }

    getAllProducts() {

        let me = this;
        return co(function*() {
            try {
                var pr = [];
                var category = ["Phone",
                    "Fridge",
                    "Washing Machine",
                    "Oven",
                    "Microwave",
                    "Tablet",
                    "Television",
                    "Air Conditioner"
                ];
                for (var i = 0; i < 8; i++)
                    yield db.Product.filter({
                        category: category[i]
                    }).limit(4).run().then(function(products) {
                        pr.push({
                            "category": category[i],
                            "pdt": products,
                            "type": "Category",
                            id: "Category" + category[i]
                        });
                    });
                return pr;

            } catch (err) {
                console.log(err);
                return false;
            }
        });
    }
    
    getOneProduct(id) {

        return co(function*() {
            try {

                var pr = yield db.Product.get(Number(id)).run();
                return pr;

            } catch (err) {
                return ({
                    "status": 404
                });
            }
        });

    }

    getTypeProducts(type, name) {

        let me = this;
        var uid = type + name;
        var listType = {};
        return co(function*() {
            try {
                var pr = yield db.Product.filter({
                    [type.toLowerCase()]: name
                }).limit(4).run().then();
                listType = {
                    "category": name,
                    "pdt": pr,
                    id: uid,
                    "type": type
                };
                return listType;
            } catch (err) {
                console.log(err);
            }
        });

    }

    filterList() {
        let me = this;
        return co(function*() {
            try {
                var series = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J"];
                var category = ["Phone", "Fridge", "Washing Machine", "Oven", "Microwave", "Tablet", "Television", "Air Conditioner"];
                var color = ["Raging Red", "Blazing Black", "Coral White", "Dark Blue", "Emerald Green", "Floral Pink", "Glimmering Gold", "Hot Pink", "Intelligent Indigo", "Jiggering Jade"];
                var size = ["XXS", "XS", "S", "M", "L", "XL", "XXL"];
                var pr = [];
                pr.push({
                    "id": "Series",
                    "array": series
                });
                pr.push({
                    "id": "Category",
                    "array": category
                });
                pr.push({
                    "id": "Color",
                    "array": color
                });
                pr.push({
                    "id": "Size",
                    "array": size
                });
                return pr;

            } catch (err) {
                console.log(err);
            }
        });

    }

    addProductsToList(body) {


        return co(function*() {

            try {
                var messageToShow = true;
                var k = yield r.uuid().run();
                body.id = k;
                body.date = new Date();
                var productData = yield db.Product.get(Number(body.pid)).run();

                if (!productData)
                    return false;

                body.products = productData;

                if (body.type == "buy") {
                    productData.sold = productData.sold + 1;
                    var saveProduct = db.Product.save(productData, {
                        conflict: 'update'
                    });
                    var userData = yield db.User.get(body.uid).run();
                    var curPoints = userData.points;
                    if (!curPoints)
                        messageToShow = "You bought " + productData.model + " " + productData.category + " at the cost of Rs. " + productData.price.toString();

                    else if (curPoints <= productData.price) {
                        body.price = productData.price - curPoints;
                        userData.points = 0;
                        messageToShow = "Yay! You bought " + productData.model + " " + productData.category + " for discount of Rs.  " + curPoints.toString() + " at Rs. " + body.price.toString();
                        db.User.save(userData, {
                            conflict: 'update'
                        });
                    } else {

                        body.price = curPoints - productData.price;
                        userData.points = body.price;
                        messageToShow = "Yay! You bought " + productData.model + " " + productData.category + " for discount of Rs. " + productData.price.toString() + " at Rs. 0";
                        db.User.save(userData, {
                            conflict: 'update'
                        });

                    }

                    var id = yield r.uuid().run();
                    var message = {};

                    if (!id)
                        return false;

                    message.id = id;
                    message.pid = id;
                    message.date = new Date();
                    message.status = "Information";
                    message.type = "Order";
                    message.receiverID = body.uid;
                    message.senderID = "Admin";
                    message.message = messageToShow;
                    message.product = productData.category + " " + productData.model;

                    var result = yield db.Message.save(message);
                    if (!result)
                        return false;
                }

                var result = yield db.Preferences.save(body);

                if (result)
                    return messageToShow;
                else
                    return false;
            } catch (err) {
                return false;
            }

        });

    }

    getProductsFromListUser(type, body) {

        return co(function*() {

            var result = false;

            var results = yield db.Preferences.filter({
                type: type,
                uid: body.uid
            }).orderBy(r.desc('date')).run().then(function(user) {
                result = user;
            });

            return result;


        });

    }

    getProductsFromListProduct(type, body) {

        return co(function*() {

            var result = false;

            var results = yield db.Preferences.filter({
                type: type,
                pid: body.pid
            }).run().then(function(user) {
                result = user;
            });

            return result;


        });

    }

    removePref(uid, pid, type) {

        return co(function*() {

            var result = false;
            var results = yield db.Preferences.filter({
                pid: Number(pid),
                uid: uid,
                type: type
            }).run()

            yield db.Preferences.get(results[0].id).then(function(pref) {
                pref.delete().then(function(result) {
                    pref.isSaved();
                });
            });

            result = true;

            return result;

        });


    }
}

let P = new productModel();
module.exports = P;