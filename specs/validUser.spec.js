var u = require('../models/user');

describe('Validating username and password for user 1 ', function () {

    it('Valid username/password combo provided', function () {
        

        var result=u.validUser('User1','User1');
        return Promise.resolve(result)
             .then(function(m) {  expect(m).to.equal(true); })
             .catch(function(m) { throw new Error('was not supposed to fail'); })
                ;
    });

    it('Existing username wrong password combo provided', function () {
        

        var result=u.validUser('User1','gmails');
        return Promise.resolve(result)
             .then(function(m) { expect(m).to.equal(false); })
             .catch(function(m) { throw new Error('was not supposed to fail'); })
                ;
    });

    it('UnExisting username wrong password combo provided', function () {
        

        var result=u.validUser('zzz','gmails');
        return Promise.resolve(result)
             .then(function(m) { expect(m).to.equal(false); })
             .catch(function(m) { throw new Error('was not supposed to fail'); })
                ;
    });

    it('Missing parameters provided - No password', function () {
        

        var result=u.validUser('1');
        return Promise.resolve(result)
             .then(function(m) { expect(m).to.be.false; })
             .catch(function(m) { throw new Error('was not supposed to fail'); })
                ;
    });

    it('Missing parameters provided - No username and password', function () {
        

        var result=u.validUser();
        return Promise.resolve(result)
             .then(function(m) { expect(m).to.equal(false); })
             .catch(function(m) { throw new Error('was not supposed to fail'); })
                ;
    });

});
