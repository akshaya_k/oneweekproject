var u = require('../models/user');
var chai=require('chai');

describe('Retrieving Badge and Points from User ', function () {

    it('Valid ID for retrieving points from User', function () {
        
        var expResultIntermediate={
                        "badge":  "Novice" ,
                        "points": 0
                      };
        var expResult=JSON.stringify(expResultIntermediate);


        var result=u.getPoints('User23');

        return Promise.resolve(result)
             .then(function(m) {   chai.expect(JSON.stringify(m)).to.equal(expResult); })
             .catch(function(m) {  throw new Error('was not supposed to fail'); })
                
    });

     it('InValid ID for retrieving points from User', function () {
        
        var expResultIntermediate={badge:'Unavailable', points: -1};
        var expResult=JSON.stringify(expResultIntermediate);
        var result=u.getPoints('User1000');

        return Promise.resolve(result)
             .then(function(m) {   chai.expect(JSON.stringify(m)).to.equal(expResult); })
             .catch(function(m) {  throw new Error('was not supposed to fail'); })
                
    });


    it('No Parameter', function () {
        
        var expResultIntermediate={badge:'Unavailable', points: -1};
        var expResult=JSON.stringify(expResultIntermediate);
        var result=u.getPoints();
       
        return Promise.resolve(result)
             .then(function(m) { chai.expect(JSON.stringify(m)).to.equal(expResult); })
             .catch(function(m) { throw new Error('was not supposed to fail'); })
                
    });

});

 