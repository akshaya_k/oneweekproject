var user = require('../models/user');
var db = require('../models/db-connection')
var chai = require('chai');
var thinky = require('thinky')({
    db: 'eCommerce3'
});
var r = thinky.r;
var type = thinky.type;
var Errors = thinky.Errors;

describe('Test Sign Up ', function () {

    before(function(){
        
        db.User.get("Test1").delete().run();
        db.User.get("Test2").delete().run();
        db.User.get("Test3").delete().run();
        //db.User.get("Test4").delete().run();
        db.User.get("Test5").delete().run();
        //db.User.get("Test6").delete().run();
        db.User.get("Test7").delete().run();
        //db.User.get("Test8").delete().run();
        db.User.get("Test9").delete().run();

    })

    it('Passing All Details', function () {
        
        var u={
            id: "Test1",
            name: "Q1571",
            location: "Location",
            email: "Q151@samsung.com",
            phone: 1234567891,
            password: "Q151"
        };
    
        var result=user.signUp(u);
             
        return Promise.resolve(result)
             .then(function(m) {   chai.expect(m).to.equal(true); })
             .catch(function(m) {  console.log(m); throw new Error('was not supposed to fail'); })
                
    });

    it('Passing existing user ID', function () {
        
        var u={
            id: "User1",
            name: "Qe151",
            location: "Location",
            email: "Q151@samsung.com",
            phone: 1234567891,
            password: "Q151"
        };

        var result=user.signUp(u);

        return Promise.resolve(result)
             .then(function(m) {   chai.expect(m).to.equal(false); })
             .catch(function(m) {  throw new Error('was not supposed to succeed'); })
                
    });

    it('Missing name', function () {
        
       var u={
            id: "Test2",
            location: "Location",
            email: "Q151@samsung.com",
            phone: 1234567891,
            password: "Q151"
        };
        
        var result=user.signUp(u);

        return Promise.resolve(result)
             .then(function(m) {   chai.expect(m).to.equal(true); })
             .catch(function(m) {  throw new Error('was not supposed to fail'); })
                
    });

    it('Missing Location', function () {
        
        var u={
            id: "Test3",
            name: "Q151",
            email: "Q151@samsung.com",
            phone: 1234567891,
            password: "Q151"
        };
        
        var result=user.signUp(u);

        return Promise.resolve(result)
             .then(function(m) {   chai.expect(m).to.equal(true); })
             .catch(function(m) {  throw new Error('was not supposed to fail'); })
                
    });

    it('Missing email', function () {
        
        var u={
            id: "Test4",
            name: "Q151",
            location: "Location",
            phone: 1234567891,
            password: "Q151"
        };
        
        var result=user.signUp(u);

        return Promise.resolve(result)
             .then(function(m) {   chai.expect(m).to.equal(false); })
             .catch(function(m) {  throw new Error('was not supposed to succeed'); })
                
    });

    it('Missing Phone', function () {
        
        var u={
            id: "Test5",
            name: "Q151",
            location: "Location",
            email: "Q151@samsung.com", 
            password: "Q151"
        };
        
        var result=user.signUp(u);

        return Promise.resolve(result)
             .then(function(m) {   chai.expect(m).to.equal(true); })
             .catch(function(m) {  throw new Error('was not supposed to fail'); })
                
    });

    it('Missing Password', function () {
        
        var u={
            id: "Test6",
            name: "Q151",
            location: "Location",
            email: "Q151@samsung.com",
            phone: 1234567891,
        };
        
        var result=user.signUp(u);

        return Promise.resolve(result)
             .then(function(m) {   chai.expect(m).to.equal(false); })
             .catch(function(m) {  throw new Error('was not supposed to succeed'); })
                
    });

    it('Missing ID', function () {
        
        var u={
            name: "ep017",
            location: "Location",
            email: "Q151@samsung.com",
            phone: 1234567891,
            password: "Q151"
        };
        
        var result=user.signUp(u);

        return Promise.resolve(result)
             .then(function(m) {   chai.expect(m).to.equal(false); })
             .catch(function(m) {  throw new Error('was not supposed to succeed'); })
                
    });

    it('missing phone location and name', function () {
        
        var u={
            id: "Test7",
            email: "Q151@samsung.com",
            password: "Q151"
        };
        
        var result=user.signUp(u);

        return Promise.resolve(result)
             .then(function(m) {   chai.expect(m).to.equal(true); })
             .catch(function(m) {  throw new Error('was not supposed to fail'); })
                
    });

    it('Missing name and location and wrong phone type', function () {
        
        var u={
            id: "Test8",
            email: "Q151@samsung.com",
            phone: "234132342",
            password: "Q151"
        };
        
        var result=user.signUp(u);

        return Promise.resolve(result)
             .then(function(m) {   chai.expect(m).to.equal(false); })
             .catch(function(m) {  throw new Error('was not supposed to succeed'); })
                
    });

    it('Missing location and name', function () {
        
         var u={
            id: "Test9",
            email: "Q151@samsung.com",
            phone: 23242323,
            password: "Q151"
        };
        
        var result=user.signUp(u);

        return Promise.resolve(result)
             .then(function(m) {   chai.expect(m).to.equal(true); })
             .catch(function(m) {  throw new Error('was not supposed to fail'); })
                
    });

    it('Passing Invalid Parameter (Non Object)', function () {
        
         var u ="Hi";
        
        var result=user.signUp(u);

        return Promise.resolve(result)
             .then(function(m) {   chai.expect(m).to.equal(false); })
             .catch(function(m) {  throw new Error('was not supposed to succeed'); })
                
    });

});