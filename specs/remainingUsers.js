
var u = require('../models/user');
var db = require('../models/db-connection')
var co = require('co')
var thinky = require('thinky')({
    db: 'eCommerce3'
});



describe('Send Message Function ', function () {

    it('No parameters', function () {
        
        var expResult=false;

        var result=u.sendMessage();

        return Promise.resolve(result)
             .then(function(m) { expect(m).to.equal(expResult); })
             .catch(function(m) { throw new Error('was not supposed to succeed'); })
                ;
    });

     it('Parameter not an object', function () {
        
        var expResult=false;
        var para="Parameter";
        var result=u.sendMessage(para);
        
        return Promise.resolve(result)
             .then(function(m) { expect(m).to.equal(expResult); })
             .catch(function(m) { throw new Error('was not supposed to succeed'); })
                ;
    });

     it('All Correct Parameters', function () {

         var messageData={
            message: "Hi",
            receiverID: "User2",
            senderID: "User1",
            type:"Ask"
         }
        
        var expResult="Hi";
        var result=u.sendMessage(messageData);
        
        return Promise.resolve(result)
             .then(function(m) { expect(m).to.equal(expResult); })
             .catch(function(m) { throw new Error('was not supposed to fail'); })
                ;
    });

    it('Sender and Receiver has same ID', function () {

         var messageData={
            message: "Hi",
            receiverID: "User1",
            senderID: "User1",
            type:"Ask"
         }
        
        var expResult=false;
        var result=u.sendMessage(messageData);
        
        return Promise.resolve(result)
             .then(function(m) { expect(m).to.equal(expResult); })
             .catch(function(m) { throw new Error('was not supposed to succeed'); })
                ;
    });

    it('Sender ID is not existing in DB', function () {

         var messageData={
            message: "Hi",
            receiverID: "User1",
            senderID: "User190",
            type:"Ask"
         }
        
        var expResult=false;
        var result=u.sendMessage(messageData);
        
        return Promise.resolve(result)
             .then(function(m) { expect(m).to.equal(expResult); })
             .catch(function(m) { throw new Error('was not supposed to succeed'); })
                ;
    });

    it('Receiver ID is not existing in DB', function () {

         var messageData={
            message: "Hi",
            receiverID: "User190",
            senderID: "User1",
            type:"Ask"
         }
        
        var expResult=false;
        var result=u.sendMessage(messageData);
        
        return Promise.resolve(result)
             .then(function(m) { expect(m).to.equal(expResult); })
             .catch(function(m) { throw new Error('was not supposed to succeed'); })
                ;
    });

     it('Any Missing Property in Parameter Object', function () {

         var messageData={
            message: "Hi",
            senderID: "User1",
            type:"Ask"
         }
        
        var expResult=false;
        var result=u.sendMessage(messageData);
        
        return Promise.resolve(result)
             .then(function(m) { expect(m).to.equal(expResult); })
             .catch(function(m) { throw new Error('was not supposed to succeed'); })
                ;
    });

});


describe('Reply Message Function ', function () {

    it('No parameters', function () {
        
        var expResult=false;

        var result=u.replyMessage();

        return Promise.resolve(result)
             .then(function(m) { expect(m).to.equal(expResult); })
             .catch(function(m) { throw new Error('was not supposed to succeed'); })
                ;
    });

     it('Parameter not an object', function () {
        
        var expResult=false;
        var para="Parameter";
        var result=u.replyMessage(para);
        
        return Promise.resolve(result)
             .then(function(m) { expect(m).to.equal(expResult); })
             .catch(function(m) { throw new Error('was not supposed to succeed'); })
                ;
    });

     it('All Correct Parameters', function () {

         var messageData={
            message: "Hi",
            receiverID: "User2",
            senderID: "User1",
            type:"Reply",
            points: 25

         }
        
        var expResult="Hi";
        var result=u.replyMessage(messageData);
        
        return Promise.resolve(result)
             .then(function(m) { expect(m).to.equal(expResult); })
             .catch(function(m) { throw new Error('was not supposed to fail'); })
                ;
    });

    it('Sender and Receiver has same ID', function () {

         var messageData={
            message: "Hi",
            receiverID: "User1",
            senderID: "User1",
            type:"Reply",
            points: 25
         }
        
        var expResult=false;
        var result=u.replyMessage(messageData);
        
        return Promise.resolve(result)
             .then(function(m) { expect(m).to.equal(expResult); })
             .catch(function(m) { throw new Error('was not supposed to succeed'); })
                ;
    });

    it('Sender ID is not existing in DB', function () {

         var messageData={
            message: "Hi",
            receiverID: "User1",
            senderID: "User190",
            type:"Reply",
            points: 25
         }
        
        var expResult=false;
        var result=u.replyMessage(messageData);
        
        return Promise.resolve(result)
             .then(function(m) { expect(m).to.equal(expResult); })
             .catch(function(m) { throw new Error('was not supposed to succeed'); })
                ;
    });

    it('Receiver ID is not existing in DB', function () {

         var messageData={
            message: "Hi",
            receiverID: "User190",
            senderID: "User1",
            type:"Reply",
            points: 25
         }
        
        var expResult=false;
        var result=u.replyMessage(messageData);
        
        return Promise.resolve(result)
             .then(function(m) { expect(m).to.equal(expResult); })
             .catch(function(m) { throw new Error('was not supposed to succeed'); })
                ;
    });

     it('Any Missing Property in Parameter Object', function () {

         var messageData={
            message: "Hi",
            senderID: "User1",
            type:"Reply",
            points: 25
         }
        
        var expResult=false;
        var result=u.replyMessage(messageData);
        
        return Promise.resolve(result)
             .then(function(m) { expect(m).to.equal(expResult); })
             .catch(function(m) { throw new Error('was not supposed to succeed'); })
                ;
    });

});

describe('Share Message Function ', function () {

    it('No parameters', function () {
        
        var expResult=false;

        var result=u.shareMessage();

        return Promise.resolve(result)
             .then(function(m) { expect(m).to.equal(expResult); })
             .catch(function(m) { throw new Error('was not supposed to succeed'); })
                ;
    });

    it('1 out of 2 parameters', function () {
        
        var expResult=false;

        var result=u.shareMessage("hi");

        return Promise.resolve(result)
             .then(function(m) { expect(m).to.equal(expResult); })
             .catch(function(m) { throw new Error('was not supposed to succeed'); })
                ;
    });

     it('Parameters not valid type', function () {
        
        var expResult=false;
        var para1={ab:"ab"};
        var para2="asd";
        var result=u.shareMessage(para1,para2);
        
        return Promise.resolve(result)
             .then(function(m) { expect(m).to.equal(expResult); })
             .catch(function(m) { throw new Error('was not supposed to succeed'); })
                ;
    });

     it('All Correct Parameters', function () {

         var messageData={
            receiverID: "User2",
            senderID: "User1",
            type:"Share"

         }
        
        var expResult="http://localhost:3000/#/product/1";
        var result=u.shareMessage("1",messageData);
        
        return Promise.resolve(result)
             .then(function(m) { expect(m).to.equal(expResult); })
             .catch(function(m) { throw new Error('was not supposed to fail'); })
                ;
    });

    it('Sender and Receiver has same ID', function () {

         var messageData={
            receiverID: "User1",
            senderID: "User1",
            type:"Share",
         }
        
        var expResult=false;
        var result=u.shareMessage("1",messageData);
        
        return Promise.resolve(result)
             .then(function(m) { expect(m).to.equal(expResult); })
             .catch(function(m) { throw new Error('was not supposed to succeed'); })
                ;
    });

    it('Sender ID is not existing in DB', function () {

         var messageData={
            receiverID: "User1",
            senderID: "User190",
            type:"Share"
         }
        
        var expResult=false;
        var result=u.shareMessage("1",messageData);
        
        return Promise.resolve(result)
             .then(function(m) { expect(m).to.equal(expResult); })
             .catch(function(m) { throw new Error('was not supposed to succeed'); })
                ;
    });

    it('Receiver ID is not existing in DB', function () {

         var messageData={
            receiverID: "User190",
            senderID: "User1",
            type:"Share",
         }
        
        var expResult=false;
        var result=u.shareMessage("1",messageData);
        
        return Promise.resolve(result)
             .then(function(m) { expect(m).to.equal(expResult); })
             .catch(function(m) { throw new Error('was not supposed to succeed'); })
                ;
    });

     it('Any Missing property in parameter object', function () {

         var messageData={
            senderID: "User1",
            type:"Share",
         }
        
        var expResult=false;
        var result=u.shareMessage("1",messageData);
        
        return Promise.resolve(result)
             .then(function(m) { expect(m).to.equal(expResult); })
             .catch(function(m) { throw new Error('was not supposed to succeed'); })
                ;
    });

});

describe('Resolve Message Function ', function () {

    it('No parameters', function () {
        
        var expResult=false;

        var result=u.resolveMessage();

        return Promise.resolve(result)
             .then(function(m) { expect(m).to.equal(expResult); })
             .catch(function(m) { throw new Error('was not supposed to succeed'); })
                ;
    });

    it('Valid Parent thread Message', function () {
        
        var expResult=true;

        var result=u.resolveMessage("00071b52-f74d-431b-a69c-766f1ff108dd");

        return Promise.resolve(result)
             .then(function(m) { expect(m).to.equal(expResult); })
             .catch(function(m) { throw new Error('was not supposed to fail'); })
                ;
    });

    it('InValid Parent thread Message', function () {
        
        var expResult=false;

        var result=u.resolveMessage("uughjbnb");

        return Promise.resolve(result)
             .then(function(m) {  expect(m).to.equal(expResult); })
             .catch(function(m) { throw new Error('was not supposed to succeed'); })
                ;
    });

});

describe('Unresolve Message Function ', function () {

    it('No parameters', function () {
        
        var expResult=false;

        var result=u.unresolveMessage();

        return Promise.resolve(result)
             .then(function(m) { expect(m).to.equal(expResult); })
             .catch(function(m) { throw new Error('was not supposed to succeed'); })
                ;
    });

    it('Valid Parent thread Message', function () {
        
        var expResult=true;

        var result=u.unresolveMessage("00071b52-f74d-431b-a69c-766f1ff108dd");

        return Promise.resolve(result)
             .then(function(m) { expect(m).to.equal(expResult); })
             .catch(function(m) { throw new Error('was not supposed to fail'); })
                ;
    });

    it('InValid Parent thread Message', function () {
        
        var expResult=false;

        var result=u.unresolveMessage("uughjbnb");

        return Promise.resolve(result)
             .then(function(m) {  expect(m).to.equal(expResult); })
             .catch(function(m) { throw new Error('was not supposed to succeed'); })
                ;
    });

});

describe('Get All Messages Function ', function () {

    it('No parameters', function () {
        
        var expResult=false;

        var result=u.getAllMessage();

        return Promise.resolve(result)
             .then(function(m) { expect(m).to.equal(expResult); })
             .catch(function(m) { throw new Error('was not supposed to succeed'); })
                ;
    });

    it('Correct sender ID', function () {
        
        var expResult='[{"group":"d2a40a6e-a5dc-4ce5-9d7b-b1b3527d5fa2","reduction":[{"date":"2016-07-30T16:06:41.102Z","id":"d2a40a6e-a5dc-4ce5-9d7b-b1b3527d5fa2","message":"http://localhost:3000/#/product/48","pid":"d2a40a6e-a5dc-4ce5-9d7b-b1b3527d5fa2","product":"Phone I3","receiverID":"User4","senderID":"User100","status":"Unresolved","type":"Share"}]}]';

        var result=u.getAllMessage("User4");

        return Promise.resolve(result)
             .then(function(m) { expect(JSON.stringify(m)).to.equal(expResult); })
             .catch(function(m) { throw new Error('was not supposed to fail'); })
                ;
    });

     it('inCorrect sender ID', function () {
        
        var expResult=false;

        var result=u.getAllMessage("User88");

        return Promise.resolve(result)
             .then(function(m) { expect(m).to.equal(expResult); })
             .catch(function(m) {  throw new Error('was not supposed to succeed'); })
                ;
    });

});

describe('If Bought Function ', function () {

    it('No parameters', function () {
        
        var expResult=false;

        var result=u.ifBought();

        return Promise.resolve(result)
             .then(function(m) { expect(m).to.equal(expResult); })
             .catch(function(m) { throw new Error('was not supposed to succeed'); })
                ;
    });

    it('Correct sender ID', function () {
        
        var expResult=[{
                        "date": "2016-08-01T03:40:13.642Z" ,
                        "id":  "9aa7ccd3-5eb2-4112-b5f1-078c881b43a2" ,
                        "pid": 58 ,
                        "products": {
                                        "category":  "Washing Machine" ,
                                        "color":  "Intelligent Indigo" ,
                                        "id": 58 ,
                                        "image":  "Washing Machine.jpg" ,
                                        "model":  "I4" ,
                                        "price": 589999 ,
                                        "series":  "I" ,
                                        "size":  "S" ,
                                        "sold": 1
                                    } ,
                        "type":  "buy" ,
                        "uid":  "User1"
                        }];

        var result=u.ifBought("User1","58");
        expResult=JSON.stringify(expResult);

        return Promise.resolve(result)
             .then(function(m) { expect(JSON.stringify(m)).to.equal(expResult); })
             .catch(function(m) { throw new Error('was not supposed to fail'); })
                ;
    });

});

describe('Get All Answers Function ', function () {

    it('Valid ID', function () {
        
        var expResult=[
                        {"pp":50,"q":"What color product did you buy?","a":"Black","p":"G7","c":"Phone"},
                        {"pp":100,"q":"Did you have a satisfying range of products to choose from?","a":"asdfasd","p":"G7","c":"Phone"},
                        {"pp":100,"q":"How was your buying experience?","a":"gooood","p":"G7","c":"Phone"},
                        {"pp":50,"q":"Would you reccommend this product to family members?","a":"afs","p":"A2","c":"Microwave"},
                        {"pp":50,"q":"Would you reccommend this product to family members?","a":"Yesss","p":"F8","c":"Oven"}
                    ];
        expResult=JSON.stringify(expResult);

        var result=u.getAllAnswers("User10");

        return Promise.resolve(result)
             .then(function(m) { expect(JSON.stringify(m)).to.equal(expResult); })
             .catch(function(m) {  throw new Error('was not supposed to fail'); })
                ;
    });

    it('No parameters', function () {
        
        var expResult = false;

        var result=u.getAllAnswers();

        return Promise.resolve(result)
             .then(function(m) { expect(m).to.equal(expResult); })
             .catch(function(m) {  throw new Error('was not supposed to succeed'); })
                ;
    });

   
});

describe('Answer One Question Function ', function () {


    it('No parameters', function () {
        
        var expResult = false;

        var result=u.answerOneQuestion();

        return Promise.resolve(result)
             .then(function(m) { expect(m).to.equal(expResult); })
             .catch(function(m) { throw new Error('was not supposed to succeed'); })
                ;
    });

    
     it('Valid Parameters', function () {
        
        var expResult = true;

        var answer={uid:"User1",qid:"1",message:"Yes"};

        var result=u.answerOneQuestion(answer);

        return Promise.resolve(result)
             .then(function(m) { expect(m).to.equal(expResult); })
             .catch(function(m) { throw new Error('was not supposed to fail'); })
                ;
    });

   
});

describe('Get One Question Function ', function () {


    it('No parameters', function () {
        
        var expResult = false;

        var result=u.getOneQuestion();

        return Promise.resolve(result)
             .then(function(m) { expect(m).to.equal(expResult); })
             .catch(function(m) {  throw new Error('was not supposed to succeed'); })
                ;
    });

    
     it('Valid Parameters', function () {
        
        var expResult = {
                            "id":507,
                            "pid":64,
                            "points":50,
                            "question":"Would you reccommend this product to family members?",
                            "type":"Daily",
                            "name":"E1 Phone"
                        }

        expResult=JSON.stringify(expResult);
        var result=u.getOneQuestion("ab","Daily");

        return Promise.resolve(result)
             .then(function(m) { expect(JSON.stringify(m)).to.equal(expResult); })
             .catch(function(m) {  throw new Error('was not supposed to fail'); })
                ;
    });

   
});

describe('Get Experts Function ', function () {


    it('No parameters', function () {
        
        var expResult = false;

        var result=u.getExperts();

        return Promise.resolve(result)
             .then(function(m) { expect(m).to.equal(expResult); })
             .catch(function(m) {  throw new Error('was not supposed to succeed'); })
                ;
    });

     it('Valid Parameters', function () {
        
        var expResult = [ { id: 'User100' }, { id: 'User1' }, { id: 'User9' } ];
        expResult=JSON.stringify(expResult);
        var result=u.getExperts("1","User16");

        return Promise.resolve(result)
             .then(function(m) { expect(JSON.stringify(m)).to.equal(expResult); })
             .catch(function(m) {  throw new Error('was not supposed to fail'); })
                ;
    });

    
    
   
});

describe('Get Users Function ', function () {


    it('No parameters', function () {
        
        var expResult = false;

        var result=u.getUsers();
        

        return Promise.resolve(result)
             .then(function(m) { expect(m).to.equal(expResult); })
             .catch(function(m) {  throw new Error('was not supposed to succeed'); })
                ;
    });

     it('Valid Parameters', function () {
        
        var result=u.getUsers("User16");
        var expResult = true;;
        
        return Promise.resolve(result)
        .then(function(m) {  
            var currResult;
            for(var i=0; i < m.length; i++){
                if(m[i].id=="User16"){
                    currResult=false;
                    break;
                }
            }

            if(currResult==undefined)
                currResult=true;
            
            expect(expResult).to.equal(currResult); 
        })
        .catch(function(m) { console.log(m); throw new Error('was not supposed to fail'); })
            ;
    

    });

    
    
   
});


