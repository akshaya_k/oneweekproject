
var u = require('../models/user');
var chai = require('chai');

chai.config.includeStack = true;

global.expect = chai.expect;
global.AssertionError = chai.AssertionError;
global.Assertion = chai.Assertion;
global.assert = chai.assert;

describe('Checking if ID exists for given ID ', function () {

    it('Valid User ID', function () {
        
        var expResult=true;

        var result=u.checkForUser('User43');
        return Promise.resolve(result)
             .then(function(m) { expect(m).to.equal(expResult); })
             .catch(function(m) { throw new Error('was not supposed to fail'); })
                ;
    });

    it('InValid User ID', function () {
        
        var expResult=false;

        var result=u.checkForUser('User60');
        return Promise.resolve(result)
             .then(function(m) { expect(m).to.equal(expResult); })
             .catch(function(m) { throw new Error('was not supposed to succeed'); })
                ;
    });

    it('InValid ID type', function () {
        
        var expResult=false;

        var result=u.checkForUser({badge:"Expert"});
        return Promise.resolve(result)
             .then(function(m) { expect(m).to.equal(expResult); })
             .catch(function(m) { throw new Error('was not supposed to succeed'); })
                ;
    });

    it('No Parameter', function () {
        
        var expResult=false;

        var result=u.checkForUser();
        return Promise.resolve(result)
             .then(function(m) { expect(m).to.equal(expResult); })
             .catch(function(m) { throw new Error('was not supposed to succeed'); })
                ;
    });
});