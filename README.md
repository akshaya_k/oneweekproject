# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###


eCommerce Websites are used for the purpose of viewing and buying a product by most customers.
Only some of the customers are loyal to that eCommerce Company.
Most just compare the prices and opt for the most competitive one.
Once a transaction is made, the customer comes back only for another transaction.
There is usually a long time gap in between.

The MAIN purpose of the project:
        To reduce this time gap between transactions
            Creating a more interactive platform to make the customers keep coming back
        Induce customer Loyalty

Project Objective:
    To create an interactive eCommerce website

USECASES:

    1. Sign up
    2. Login
    3. Search Products
    4. View Product Details
    5. Add to Cart
    6. WishList
    7. Shortlist
    8. Buy Product
    9. Share Product Details through Message
    10. Message View
    11. Dashboard - view 5,6,7, Recently Bought, Messages
    12. Answer questions on buying a product to earn points
    13. Become an expert after getting 500 points and answer questions from any user.

### How do I get set up? ###
* Project Directory Structure


FRONT - END

	Partials - html files
	Controllers


		App declaration: 
			module.js

		(SI) session managament : 
			service.js

		 search filters : 
			service.js

		config - state configuration for multiple views on one page		

		user.js
			Message, questions, sign in, sign up controllers

		product:
			all product related controllers

	
BACK END

	Models
		db-connection - model creation
		user.js - user CLASS for user functions
		product.js - product CLASS for product functions

ROUTES

	routes.js
		index.js (for hosting in local host for index .html src files)
		products.js - routes for products 
		users.js - routes for users
		all - concatenated all routes into one array

assests
	image files
	style - css files 

schema 
	joi validation
	for only post methods

specs - code for unit testing

src 
	javascript files for importing



	
* Summary of set up

npm install:
hapi, joi, rethinkdb,thinky, co, nodemon, chai, mocha

* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

First go to project directory and run "rethinkdb" on one terminal
Then in the second terminal, run "nodemon server.js"
In the browser: type localhost:3000
and start using the project

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact