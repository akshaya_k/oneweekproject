
var routerApp = angular.module('routerApp', ['ngStorage', 'ui.router', 'angular.filter']);

routerApp.controller('navbarController', function($scope, $window, $localStorage, $state) {


    $scope.sessionID = $window.localStorage.getItem("currentUser");
    $scope.ifSession = $window.localStorage.getItem("result");
    $scope.signOut = () => {

        $window.localStorage.setItem("currentUser", -1);
        $window.localStorage.setItem("result", false);
        $state.go($state.current, {}, {
            reload: true
        });

    }

});