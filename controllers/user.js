
routerApp.controller('signinController', function($scope, $state, $http, SI, $rootScope, $window, $localStorage) {


    $scope.signIn = () => {

        SI.verify($scope.user.id, $scope.user.password).then((data) => {
            var id = SI.getSession();
            if (id) {
                $window.localStorage.setItem("currentUser", id);
                $window.localStorage.setItem("result", true);
                $state.go('index');
            } else {

                $window.localStorage.setItem("currentUser", false);
                $window.localStorage.setItem("result", false);
                alert("Invalid Username/Password. Both are Case Sensitive.");
            }
        });
    }



});

routerApp.controller('signupController', function($scope, $http, $state) {

    $scope.signUp = () => {
        $http({
            method: 'POST',
            url: '/register',
            data: $scope.vm
        }).then(function success(output) {
            if (output.data == true) {
                alert("Successfully Signed up! Please Login. ");
                $state.go('signin');
            } else {
                alert("User Login Already Exists!");
            }

        }, function failure(output) {
            alert("Could not process your request. Check your Phone Number/Location.");

        });
    }


});

routerApp.controller('homeController', function($scope, $rootScope, $window, $http, $localStorage) {

    $scope.sessionID = $window.localStorage.getItem("currentUser");
    $scope.ifSession = $window.localStorage.getItem("result");

    if ($scope.ifSession == 'true') {
        $http({
            method: 'GET',
            url: '/points/' + $scope.sessionID,
        }).then(function success(output) {

            if (output.data) {
                $scope.points = output.data.points;
                $scope.badge = output.data.badge;
            }

        }, function failure(output) {
            $scope.points = -1;

        });
    }



});

routerApp.controller('messageController', function($state, $http, $scope, $rootScope, $window, $localStorage) {


    $scope.sessionID = $window.localStorage.getItem("currentUser");
    $scope.ifSession = $window.localStorage.getItem("result");
    $scope.m = {};
    $scope.m.M = [];
    var name = $state.current.name;
    $scope.statusCode = 3;
    $http({
        method: 'GET',
        url: '/messages/' + $scope.sessionID,
    }).then(function success(output) {

        if ($scope.ifSession == 'false')
            $scope.statusCode = 3;
        else {
            $scope.all = output.data;
            $scope.statusCode = 1;
        }
    }, function failure(output) {

        $scope.statusCode = 2;
        console.log(output);
    });

    $scope.reply = (senderID, pid, id, i, points) => {

        var body = {};
        body.receiverID = senderID;
        body.pid = pid;
        body.senderID = $scope.sessionID;
        body.points = points;

        if ($scope.m.M.length <= 0) {
            alert("Reply Cannot be blank!");
        } else {
            body.message = $scope.m.M[i];
            body.type = "Reply";

            $http({

                method: 'POST',
                url: '/reply',
                data: body

            }).then(function success(output) {

                if (!output.data)
                    alert("Please check your input!");

                else {
                    alert("Message " + output.data + " successfully sent to " + senderID);
                    $state.go($state.current, {}, {
                        reload: true
                    });
                }

            }, function failure(output) {
                alert("Message could not be sent.");
            });
        }

    }

    $scope.resolve = (pid) => {

        $http({
            method: 'GET',
            url: '/resolve/' + pid
        }).then(function success(output) {

            if (!output.data)
                alert("Thread Invalid");

            else {
                alert("Question Marked Resolved Successfuly");
                $state.go($state.current, {}, {
                    reload: true
                });
            }

        }, function failure(output) {
            alert("Could not be resolved temporarily");
        });
    }



    $scope.unresolve = (pid) => {

        $http({
            method: 'GET',
            url: '/unresolve/' + pid
        }).then(function success(output) {
            if (!output.data)
                alert("Thread Invalid");
            else {
                alert("Question Marked Unresolved Successfuly");
                $state.go($state.current, {}, {
                    reload: true
                });
            }
        }, function failure(output) {
            alert("Could not be unresolved temporarily");
        });
    }


});


routerApp.controller('questionController', function($scope, $http, $rootScope, $window, $localStorage) {

    $scope.sessionID = $window.localStorage.getItem("currentUser");
    $scope.ifSession = $window.localStorage.getItem("result");

    $scope.setOf = {};
    $scope.setOf.toShow = 'false';

    if ($scope.ifSession == 'true') {

        $http({
            method: 'GET',
            url: '/answer/' + $scope.sessionID
        }).then(function success(output) {
            if (output.data == false) {
                // alert("Temporarily Unable to retrieve Answers");
                
                $scope.setOf.toShow = 'inbet';
            } else {
                $scope.setOf.answers = output.data;
                $scope.setOf.toShow = 'true';
            }
        }, function failure(output) {
            alert("Temporarily Unable to retrieve Answers");
            $scope.setOf.toShow = 'inbet';
        });

    }

});