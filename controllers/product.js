
routerApp.controller('productController', function($scope, $rootScope, $state, $http, filters, $window, $localStorage) {


    $scope.sessionID = $window.localStorage.getItem("currentUser");
    $scope.ifSession = $window.localStorage.getItem("result");

    $scope.qW = {};
    $scope.qD = {};
    $scope.qW.X = 'true';
    $scope.qD.X = 'true';

    if ($scope.ifSession == 'true') {
        $http({
            method: 'GET',
            url: '/question/Weekly/' + $scope.sessionID,
        }).then(function success(output) {
           if (typeof(output.data)=="string") {
                $scope.qW.X = 'false';
                $scope.qW.mX = output.data + " Buy one product to answer Question of the Week.";
            } else
                $scope.qW.s = output.data;

        }, function failure(output) {
            $scope.qW.X = 'false';
            $scope.qW.mX = output.data;
            console.log(output);

        });

        $http({
            method: 'GET',
            url: '/question/Daily/' + $scope.sessionID,
        }).then(function success(output) {
           
            if (typeof(output.data)=="string") {
                $scope.qD.X = 'false';
                $scope.qD.mX = output.data + " Buy one product to answer Question of the Day.";
            } else
                $scope.qD.s = output.data;

        }, function failure(output) {
            $scope.qD.X = 'false';
            $scope.qD.mX = output.data;
            console.log(output);

        });
    } else {
        $scope.qW.X = 'false';
        $scope.qD.X = 'false';
        // $scope.qD.mX='Sign in to answer Daily Questions and earn points!'
        //$scope.qW.mX='Sign in to answer Weekly Questions and earn points!'
    }

    $scope.answerD = () => {
        var body = {};
        body.qid = $scope.qD.s.id;
        body.uid = $scope.sessionID;
        body.message = $scope.qD.q;
        if ($scope.qD.q != undefined) {
            $http({
                method: 'POST',
                url: '/answer',
                data: body
            }).then(function success(output) {

                if(output.data){
                    alert("Successfully Answered! You earned 50 points!");
                    $state.go($state.current, {}, {
                        reload: true
                    });
                }
                else
                    alert("Could not process Answer. Try again later.");
            }, function failure(output) {
                console.log(output);
                alert("Could not process Answer. Try again later.");
            });
        } else
            alert("Answer cannot be blank.");

    }

    $scope.answerW = () => {

        var body = {};
        body.qid = $scope.qW.s.id;
        body.uid = $scope.sessionID;
        body.message = $scope.qW.q;
        if ($scope.qW.q != undefined) {

            $http({
                method: 'POST',
                url: '/answer',
                data: body
            }).then(function success(output) {
                if(output.data){
                    alert("Successfully Answered! You earned 100 points!");
                    $state.go($state.current, {}, {
                        reload: true
                    });
                }
                else
                    alert("Could not process Answer. Try again later.");

            }, function failure(output) {
                console.log(output);
                alert("Could not process Answer. Try again later.");
            });
        } else
            alert("Answer cannot be blank.");

    }

    $rootScope.$on("HelloEvent", function(event, message) {
        $scope.category = message.msg;
        if ($scope.category.length == 0) {
            $scope.category = $scope.categor;

        }
    });

    $http({
        method: 'GET',
        url: '/product',
    }).then(function success(output) {
        $scope.category = output.data;
        $scope.categor = output.data;
    }, function failure(output) {
        console.log(output);
    });

    $scope.buy = (id) => {

        $http({
            method: 'POST',
            url: '/product',
            data: {
                uid: $scope.sessionID,
                pid: id,
                type: 'buy',
            }
        }).then(function success(output) {
            if (output.data == false)
                alert("Unable to Process Transaction. Try Again Later!");
            else {
                alert(output.data);
            }
        }, function failure(output) {
            alert("Unable to Process Transaction. Try Again Later!");
        });

    }
});

routerApp.controller('productviewController', function($scope, $http, $stateParams, $state, $window, $localStorage) {


    $scope.sessionID = $window.localStorage.getItem("currentUser");
    $scope.ifSession = $window.localStorage.getItem("result");
    $scope.buying = 'false';
    $scope.experts = 0;
    $scope.listers = 0;
    $scope.toMessage = -1;
    $scope.toMessage1 = -1;

    $scope.m = {};
    $scope.disable = {};
    $scope.currentProduct = {};
    $scope.disable.next = 0;
    $scope.disable.prev = 0;

    $scope.currentProduct.next = Number($stateParams.id) + 1;
    $scope.currentProduct.prev = Number($stateParams.id) - 1;

    if ($scope.currentProduct.next > 500)
        $scope.disable.next = 1;

    else if ($scope.currentProduct.prev < 1)
        $scope.disable.prev = 1;

    $scope.option = (type, pid) => {

        $http({
            method: 'POST',
            url: '/product',
            data: {
                uid: $scope.sessionID,
                pid: pid,
                type: type,
            }
        }).then(function success(output) {
            if (type == "buy") {
                alert(output.data);
                $scope.buying = 'true';
            } else if (type == "wishlist")
                alert("Added to Wishlist Successfully!");
            else if (type == "shortlist")
                alert("Shortlisted Successfully!!");
            else if (type == "add")
                alert("Added to cart!");
        }, function failure(output) {
            alert("Unable to Process Request. Try Again Later!");
        });

    }

    $http({
        method: 'GET',
        url: '/product/' + $stateParams.id,
    }).then(function success(output) {
        if (output.data.status == 404)
            $state.go("404");
        $scope.product = output.data;
    }, function failure(output) {
        console.log(output);
    });

    $http({
        method: 'GET',
        url: '/bought/' + $scope.sessionID + '/' + $stateParams.id,
    }).then(function success(output) {

        if (output.data && output.data.length > 0)
            $scope.buying = 'true';

    }, function failure(output) {
        console.log(output);
        $scope.buying = 'false';
    });

    $http({
        method: 'GET',
        url: '/expert/' + $stateParams.id + '/' + $scope.sessionID,
    }).then(function success(output) {

        if (output.data && output.data.length > 0) {
            $scope.listofexperts = output.data;
            $scope.experts = 1;
        } else {
            $scope.experts = 2;
        }
    }, function failure(output) {
        console.log(output);
        $scope.experts = 0;
    });

    $http({
        method: 'GET',
        url: '/user/' + $scope.sessionID,
    }).then(function success(output) {
        if (output.data && output.data.length > 0) {
            $scope.listofusers = output.data;
            $scope.listers = 1;
        } else {
            $scope.listers = 2;
        }
    }, function failure(output) {
        console.log(output);
        $scope.listers = 0;
    });

    $scope.GetValue = function() {
        $scope.toMessage = $scope.m.messProp.id;
    }

    $scope.Ask = () => {

        if ($scope.toMessage == -1) {
            alert("Pick an Expert");
        } else if ($scope.toMessage == $scope.sessionID) {
            alert("Dont' ask yourself a question!");
        } else if ($scope.sessionID == -1) {
            alert("Sign in to ask a question!");
        } else {

            var body = {};
            body.product = $scope.product.category + " " + $scope.product.model;
            body.receiverID = $scope.toMessage;
            body.senderID = $scope.sessionID;
            body.message = $scope.m.M;
            body.type = "Ask";
            $http({
                method: 'POST',
                url: '/send',
                data: body
            }).then(function success(output) {
                if (!output.data)
                    alert("Please check your input!");

                else {
                    alert("Question " + output.data + " successfully asked to " + $scope.toMessage);

                }

            }, function failure(output) {
                alert("Question could not be sent temporarily!");
            });
        }
    }

    $scope.GetValues = function() {
        $scope.toMessage1 = $scope.m.messProp1.id;
    }

    $scope.Share = () => {

        if ($scope.toMessage1 == -1) {
            alert("Pick a User");
        } else if ($scope.toMessage1 == $scope.sessionID) {
            alert("Don't Share details with yourself!");
        } else if ($scope.sessionID == -1) {
            alert("Sign in to share details!!");
        } else {

            var body = {};
            body.product = $scope.product.category + " " + $scope.product.model;
            body.receiverID = $scope.toMessage1;
            body.senderID = $scope.sessionID;
            body.type = "Share";
            $http({
                method: 'POST',
                url: '/share/' + $stateParams.id,
                data: body
            }).then(function success(output) {
                if (output.data == -1)
                    alert("Product could not be shared!");

                else {
                    alert("Product " + output.data + " successfully Shared to " + $scope.toMessage1);

                }

            }, function failure(output) {
                alert("Product could not be shared!");
            });
        }

    }

});

routerApp.controller('filterlistController', function($scope, $http, filters, $rootScope, $window, $localStorage) {

    $scope.view = function(val, type, cat) {

        var uid = type + cat;

        if (val) {

            $http({
                method: 'GET',
                url: '/product/search/' + type + "/" + cat,
            }).then(function success(output) {
                filters.add(uid, output.data);
                $rootScope.$broadcast("HelloEvent", {
                    msg: filters.list()
                });
            }, function failure(output) {
                console.log(output);
            });

        } else {

            filters.remove(uid);
            $rootScope.$broadcast("HelloEvent", {
                msg: filters.list()
            });
        }
    }

    $http({
        method: 'GET',
        url: '/filter',
    }).then(function success(output) {
        $scope.filtertypes = output.data;
    }, function failure(output) {
        console.log(output);
    });

});