
routerApp.controller('commonListController', function($state, $scope, $http, $localStorage, $window) {


    $scope.sessionID = $window.localStorage.getItem("currentUser");
    $scope.ifSession = $window.localStorage.getItem("result");
    var name = $state.current.name;
    $scope.cartType = 'notBuy';
    $scope.listName;
    if (name == "home.addtocart") {

        $scope.listName = "Cart Items";
        if ($scope.ifSession == 'false')
            $scope.statusCode = 3;

        else
            $http({
                method: 'POST',
                url: '/product/user/add',
                data: {
                    uid: $scope.sessionID
                }
            }).then(function success(output) {
                if (output.data.length > 0) {
                    $scope.statusCode = 1;
                    $scope.list = output.data;
                } else {

                    $scope.statusCode = 0;
                }

            }, function failure(output) {
                $scope.statusCode = 2;
                console.log(output);
            });
    } else if (name == "home.wishlist") {

        $scope.listName = "Wish List Items";

        if ($scope.ifSession == 'false')
            $scope.statusCode = 3;

        else
            $http({
                method: 'POST',
                url: '/product/user/wishlist',
                data: {
                    uid: $scope.sessionID
                }
            }).then(function success(output) {
                if (output.data.length > 0) {

                    $scope.statusCode = 1;
                    $scope.list = output.data;
                } else {

                    $scope.statusCode = 0;
                }

            }, function failure(output) {
                $scope.statusCode = 2;
                console.log(output);
            });
    } else if (name == "home.shortlist") {


        $scope.listName = "Short Listed Items";

        if ($scope.ifSession == 'false')
            $scope.statusCode = 3;

        else
            $http({
                method: 'POST',
                url: '/product/user/shortlist',
                data: {
                    uid: $scope.sessionID
                }
            }).then(function success(output) {
                if (output.data.length > 0) {

                    $scope.statusCode = 1;
                    $scope.list = output.data;
                } else {

                    $scope.statusCode = 0;
                }

            }, function failure(output) {
                $scope.statusCode = 2;
                console.log(output);
            });
    } else if (name == "home.bought") {

        $scope.listName = "Recently Bought Items";
        $scope.cartType = "buy";

        if ($scope.ifSession == 'false')
            $scope.statusCode = 3;

        else
            $http({
                method: 'POST',
                url: '/product/user/buy',
                data: {
                    uid: $scope.sessionID
                }
            }).then(function success(output) {
                if (output.data.length > 0) {

                    $scope.statusCode = 1;
                    $scope.list = output.data;
                } else {

                    $scope.statusCode = 0;
                }

            }, function failure(output) {
                $scope.statusCode = 2;
                console.log(output);
            });
    } else if (name == "home.messages") {

        $scope.listName = "Messages";

        if ($scope.ifSession == 'false')
            $scope.statusCode = 3;

        else
            $scope.statusCode = 2;

    } else if (name == "home.answers") {


        $scope.listName = "Points";

        if ($scope.ifSession == 'false')
            $scope.statusCode = 3;

        else
            $scope.statusCode = 2;


    }

    $scope.buy = (id) => {

        $http({
            method: 'POST',
            url: '/product',
            data: {
                uid: $scope.sessionID,
                pid: id,
                type: 'buy',
            }
        }).then(function success(output) {
            alert(output.data);
            $state.go($state.current, {}, {
                reload: true
            });
        }, function failure(output) {
            alert("Unable to Process Transaction. Try Again Later!");
        });

    }

    $scope.remove = (id) => {

        var type;
        if (name == "home.addtocart") {
            type = "add";
        } else if (name == "home.wishlist") {
            type = "wishlist";
        } else if (name == "home.shortlist") {
            type = "shortlist";
        } else if (name == "home.bought") {
            type = "buy";
        }

        $http({
            method: 'DELETE',
            url: '/preference/' + $scope.sessionID + '/' + type + '/' + id,
        }).then(function success(output) {
            $state.go($state.current, {}, {
                reload: true
            });
        }, function failure(output) {
            console.log(output);
            alert("Sorry! Try again later!");
        });


    }

});