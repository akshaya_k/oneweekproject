
routerApp.factory('filters', function() {
    var items = [];
    var itemsService = {};

    itemsService.add = function(uid, item) {
        items.push(item);

    };

    itemsService.remove = function(uid) {

        var i;
        for (i = 0; i < items.length; i++) {

            if (items[i].id == uid) {
                items.splice(i, 1);
                break;
            }

        }
    };

    itemsService.list = function() {
        return items;
    };

    return itemsService;
});


routerApp.factory('SI', ['$http', function($http) {
    var userID;
    var signService = {};

    signService.verify = function(user, password) {

        var promise = $http({
            method: 'POST',
            url: '/login',
            data: {
                id: user,
                password: password
            }
        }).then(function success(output) {
            if (output.data)
                userID = user;
            else
                userID = false;

        }, function failure(output) {
            console.log(output);
            alert("boo");

        });

        return promise;

    };

    signService.getSession = function() {
        return userID;
    }

    return signService;
}]);