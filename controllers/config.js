
routerApp.config(function($stateProvider, $urlRouterProvider) {

    $urlRouterProvider.otherwise('/');

    $stateProvider

        .state('index', {
        url: '/',
        views: {
            nav: {
                templateUrl: 'partials/navbar.html',
                controller: 'navbarController'
            },
            content: {
                templateUrl: 'partials/products.html',
                controller: 'productController'
            }
        }
    })

    .state('home', {
        url: '/home',
        views: {
            'content': {
                templateUrl: 'partials/partial-home.html',
                controller: 'homeController'
            },
            'nav': {
                templateUrl: 'partials/navbar.html',
                controller: 'navbarController'
            }
        }
    })


    .state('home.addtocart', {
        url: '/cart',
        views: {
            'tab': {
                templateUrl: 'partials/commonlist.html',
                controller: 'commonListController'
            }
        }
    })

    .state('home.wishlist', {
        url: '/wishlist',
        views: {
            'tab': {
                templateUrl: 'partials/commonlist.html',
                controller: 'commonListController'
            }
        }
    })

    .state('home.shortlist', {
        url: '/shortlist',
        views: {
            'tab': {
                templateUrl: 'partials/commonlist.html',
                controller: 'commonListController'
            }
        }
    })

    .state('home.bought', {
        url: '/bought',
        views: {
            'tab': {
                templateUrl: 'partials/commonlist.html',
                controller: 'commonListController'
            }
        }
    })

    .state('home.messages', {
        url: '/messages',
        views: {
            'tab': {
                templateUrl: 'partials/messageview.html',
                controller: 'messageController'
            }
        }
    })

    .state('home.answers', {
        url: '/answers',
        views: {
            'tab': {
                templateUrl: 'partials/questions.html',
                controller: 'questionController'
            }
        }
    })

    .state('product', {
        url: '/product/{id}',
        views: {
            content: {
                templateUrl: 'partials/productview.html',
                controller: 'productviewController'
            },
            nav: {
                templateUrl: 'partials/navbar.html',
                controller: 'navbarController'
            }
        }

    })

    .state('search', {
        url: '/products/search',
        views: {

            'content': {
                templateUrl: 'partials/partial-search.html',
            },


            'filtersearch@search': {
                templateUrl: 'partials/filtersearch.html',
                controller: 'filterlistController'
            },


            'listproducts@search': {
                templateUrl: 'partials/listproducts.html',
                controller: 'productController'
            },

            'nav': {
                templateUrl: 'partials/navbar.html',
                controller: 'navbarController'
            }
        }
    })

    .state('404', {
        url: '/error',
        views: {
            nav: {
                templateUrl: 'partials/navbar.html',
                controller: 'navbarController'
            },
            content: {
                templateUrl: 'partials/notfound.html',
            }
        }

    })

    .state('signin', {
        url: '/signin',
        views: {
            'content': {
                templateUrl: 'partials/signin.html',
                controller: 'signinController'
            },
            nav: {
                templateUrl: 'partials/navbar.html',
                controller: 'navbarController'
            }
        }
    })

    .state('signup', {
        url: '/register',
        views: {
            'content': {
                templateUrl: 'partials/register.html',
                controller: 'signupController'
            },
            nav: {
                templateUrl: 'partials/navbar.html',
                controller: 'navbarController'
            }
        }
    })
});