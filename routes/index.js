module.exports= [

    {
        method: 'GET',
        path: '/',
        handler: function (request, reply) {
            reply.file('index.html');
        }
    }, 
    { 
        method: 'GET',
        path: '/src/{file*}',
        handler: {
            directory: { 
            path: 'src'
            }
        }
    },
    { 
        method: 'GET',
        path: '/controllers/{file*}',
        handler: {
            directory: { 
            path: 'controllers'
            }
        }
    },
    {         
        method: 'GET',
        path: '/assets/styles/{file*}',
        handler: {
            directory: { 
            path: 'assets/styles'
            }
        }  
    },
    {         
        method: 'GET',
        path: '/assets/images/{file*}',
        handler: {
            directory: { 
            path: 'assets/images'
            }
        }  
    },
    { 
        method: 'GET',
        path: '/partials/{file*}',
        handler: {
            directory: { 
            path: 'partials'
            }
        }   
    },
     { 
        method: 'GET',
        path: '/error',
        handler: function (request, reply) {
            reply("NOT FOUND");
        },
        config: {
            tags: ['api'],
        }
    }
];