const p=require("../models/product.js");
const schema = require('../schema/schema.js');
const co=require('co');
module.exports= [ 

    //product details of given ID  
    { 
        method: 'GET',
        path: '/product/{id}',
        handler: function (request, reply) {
            p.getOneProduct(request.params.id).then((data) => {
                   
                    reply(data);
            },(err)=>{
                console.log(err);
                    reply(err);
            });  
        },
        config: {
            tags: ['api'],
            description: 'Route to product details of given ID'
        }
    },

    //products search page
    { 
        method: 'GET',
        path: '/product/search',
        handler: function (request, reply) {
            p.getAllProducts().then((data) => {
                reply(data);
            },(err)=>{
                    reply(err);
            });  
        },
        config: {
            
            tags: ['api'],
            description: 'Route to products search page'
        }
    },

    //retrieve list of products for category type and name
    { 
        method: 'GET',
        path: '/product/search/{type}/{name}',
        handler: function (request, reply) {
            p.getTypeProducts(request.params.type,request.params.name).then((data) => {
                reply(data);
            },(err)=>{
                    reply(err);
            });  
        },
        config: {
            
            tags: ['api'],
            description: 'Route to retrieve list of products for category type and name'
        }
    },

    //get list of ALL products
    {
        method: 'GET',
        path: '/product',
        handler: function (request, reply) {
            p.getAllProducts().then((data) => {
                reply(data);
            },(err)=>{
                    reply(err)
            });  
        },
        config: {

            tags: ['api'],
            description: 'Route to get list of ALL products'
        }
    },

    //filter list for product select option
    {
        method: 'GET',
        path: '/filter',
        handler: function (request, reply) {
            p.filterList().then((data) => {
                reply(data);
            },(err)=>{
                    reply(err)
            });  
        },
        config: {
            tags: ['api'],
            description: 'Route to filter list for product select option'
        }
    },

    //Add product to a list for USER 
    {
        method: 'POST',
        path: '/product',
        handler: function (request, reply) {

            reply(p.addProductsToList(request.payload));
        } ,
        config: {
            tags: ['api'],
            description: 'Route to add products to a list'
        }
    },
    //Get list of cart/shortlist/ etc items of USER
    {
        method: 'POST',
        path: '/product/user/{type}',
        handler: function (request, reply) {

            co(function*(){
                var data=yield p.getProductsFromListUser(request.params.type,request.payload);
                reply(data);
            });
        } ,
        config: {
            tags: ['api'],
            description: 'Route to Get list of cart/shortlist/ etc items of USER'
        }
    },
    //Delete a Preference
    {
        method: 'DELETE',
        path: '/preference/{uid}/{type}/{pid}',
        handler: function (request, reply) {

            co(function*(){
                var data=yield p.removePref(request.params.uid,request.params.pid,request.params.type);
                reply(data);
            });
            
            
        } ,
        config: {
            tags: ['api'],
            description: 'Route to '
        }
    }

]