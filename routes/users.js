const u=require("../models/user.js");
const schema = require('../schema/schema.js');

const co=require("co");

module.exports= [ 
    
    //USER ROUTES
    //login
    { 
        method: 'POST',
        path: '/login',
        handler: function (request, reply) {

            reply(u.validUser(request.payload.id,request.payload.password));
        },
        config: {
            tags: ['api'],
            description: 'Route to check validation of user id and password.'
        }
    },
    //register
    { 
        method: 'POST',
        path: '/register',
        handler: function (request, reply) {
            reply(u.signUp(request.payload));
    
       },
        config: {
            tags: ['api'],
            description: 'Route to validate id and sign up user.',
            validate:{
                payload:schema.userParams
            }
        }
    },

    //ROUTES FOR MESSAGES

    //Ask a question
    { 
        method: 'POST',
        path: '/send',
        handler: function (request, reply) {

                reply(u.sendMessage(request.payload));

            },
        config: {
            tags: ['api'],
            description: 'Route to Ask Question given: senderID-sender,receiverID-receiver, message',
            validate:{
                payload:schema.Message
            }
        }
    },
    //Share a product
    { 
        method: 'POST',
        path: '/share/{pid}',
        handler: function (request, reply) {
                reply(u.shareMessage(request.params.pid,request.payload));
            },
        config: {
            tags: ['api'],
            description: 'Route to share product details',
        }
    },

    //Reply to Question
    
    { 
        method: 'POST',
        path: '/reply',
        handler: function (request, reply) {
            
            reply(u.replyMessage(request.payload));

            },
        config: {
            tags: ['api'],
            description: 'Route to reply to question.',
            validate:{
                payload:schema.Message
            }
        }
    },

    //get points,badge
    { 
        method: 'GET',
        path: '/points/{id}',
        handler: function (request, reply) {

            reply(u.getPoints(request.params.id));

            },
        config: {
            tags: ['api'],
            description: 'Route to get points,badge of User.'
        }
    },
    
    //get all messages of users

    { 
        method: 'GET',
        path: '/messages/{id}',
        handler: function (request, reply) { 
                
                reply(u.getAllMessage(request.params.id));

            },
        config: {
            tags: ['api'],
            description: 'Route to get all Messages of users'
        }
    },
    //resolve question of parent id
    { 
        method: 'GET',
        path: '/resolve/{parentID}',
        handler: function (request, reply) { 
                reply(u.resolveMessage(request.params.parentID))

            },
        config: {
            tags: ['api'],
            description: 'Route to resolve all questions of given parent id'
        }
    },

    //unresolve all threads of parent question
    { 
        method: 'GET',
        path: '/unresolve/{parentID}',
        handler: function (request, reply) { 
                reply(u.unresolveMessage(request.params.parentID));
            },
        config: {
            tags: ['api'],
            description: 'Route to Unresolve all threads of a parent question.'
        }
    },
    //Check if user bought product id
    { 
        method: 'GET',
        path: '/bought/{userID}/{productID}',
        handler: function (request, reply) {
                reply(u.ifBought(request.params.userID,request.params.productID));
        },
        config: {
            tags: ['api'],
            description: 'Route to check if user bought a particular product'
        }
    },
    //get all experts for product id (retrieve users who bought product)
    { 
        method: 'GET',
        path: '/expert/{productID}/{userID}',
        handler: function (request, reply) {
                reply(u.getExperts(request.params.productID,request.params.userID));
        },
        config: {
            tags: ['api'],
            description: 'Route to retrieve all users(experts) who bought a product'
        }
    },
    //get all users in table excepting sessionid
    { 
        method: 'GET',
        path: '/user/{id}',
        handler: function (request, reply) {
                reply(u.getUsers(request.params.id));
        },
        config: {
            tags: ['api'],
            description: 'Route to get all users except current user'
        }
    },
    //QUESTIONS SYSTEM GENERATED
    
    //get one question of type daily/weekly not answered
    { 
        method: 'GET',
        path: '/question/{type}/{uid}',
        handler: function (request, reply) {
                reply(u.getOneQuestion(request.params.uid,request.params.type));
        },
        config: {
            tags: ['api'],
            description: 'Route to get one question of type daily/weekly not answered.'
        }
    },
    //Answer the given question
    { 
        method: 'POST',
        path: '/answer',
        handler: function (request, reply) {
                reply(u.answerOneQuestion(request.payload));
        },
        config: {
            tags: ['api'],
            description: 'Route to answer and update points for question type.'
        }
    },
    //get all existing answers from answer db for user.
    { 
        method: 'GET',
        path: '/answer/{id}',
        handler: function (request, reply) {
                reply(u.getAllAnswers(request.params.id));
        },
        config: {
            tags: ['api'],
            description: 'Route to get all existing answers from answer db for user.'
        }
    }
]